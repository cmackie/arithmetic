grammar Expr;

// Parser rules
stat: expr ';' ;

expr:   expr '*' expr # Multiplication  // precedence 4
    |   expr '+' expr # Addition        // precedence 3
    |   INT           # Literal         // primary (precedence 2)
    ;

// Lexer rules
INT :   [0-9]+ ;
WS  :   [ \t\n\r]+ -> skip ;
