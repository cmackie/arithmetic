# Arithmetic

Arithmetic is a REPL for a trivial arithmetic language.
It can compute any sum of products you give it.
Simply run `npm test` to download dependencies and give it a whirl.
You need to make sure you have a script named 'antlr4' on your path to run ANTLR.

## The Grammar

The grammar for this language, seen below, exemplifies ANTLR's ability to deal
with ambiguous and left recursive grammars.
It can transform it under the hood to keep your parser safe, and the programmer happy!

`S := E;`
`E := E * E | E + E | INT`

## The Compiler

The compiler for this language will give you a sense of how to implement a compiler
in ANTLR. Notice how it passes state up the parse tree through the listener context.