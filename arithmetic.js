const readline = require('readline');
const ExprCompiler = require('./compiler').ExprCompiler;

let arithmeticCompiler = new ExprCompiler();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let recPrompt = function() {
    rl.question("> ", (userin) => {
        switch (userin) {
            case "exit":
                console.log("Goodbye!");
                rl.close();
                process.exit();
                break;
            default:
                console.log("Calculating " + userin);
                let result = arithmeticCompiler.compile(userin + ';');
                console.log(userin + '=' + result);
                break;
        }
        recPrompt();
    });
};

console.log("Welcome to arithmetic!");
console.log("Type arithmetic expressions for me to solve,");
console.log("or type 'exit' to quit.");
recPrompt();
