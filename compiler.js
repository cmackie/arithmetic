const antlr4 = require('antlr4/index');
const ExprLexer = require('./gen/ExprLexer').ExprLexer;
const ExprParser = require('./gen/ExprParser').ExprParser;
const ExprListener = require('./gen/ExprListener').ExprListener;

function ExprCompiler() {
    ExprListener.call(this);
    return this;
}

ExprCompiler.prototype = Object.create(ExprListener.prototype);
ExprCompiler.prototype.constructor = ExprCompiler;

ExprCompiler.prototype.compile = function (template) {
    this._result = 0;

    const chars = new antlr4.InputStream(template);
    const lexer = new ExprLexer(chars);
    const tokens = new antlr4.CommonTokenStream(lexer);
    const parser = new ExprParser(tokens);
    parser.buildParseTrees = true;
    const tree = parser.stat();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

    return this._result;
};

// Exit a parse tree produced by ExprParser#stat.
ExprListener.prototype.exitStat = function(ctx) {
    this._result = ctx.children[0]._result;
};

// Exit a parse tree produced by ExprParser#Multiplication.
ExprListener.prototype.exitMultiplication = function(ctx) {
    ctx._result = ctx.children[0]._result * ctx.children[2]._result;
};

// Exit a parse tree produced by ExprParser#Addition.
ExprListener.prototype.exitAddition = function(ctx) {
    ctx._result = ctx.children[0]._result + ctx.children[2]._result;
};

// Exit a parse tree produced by ExprParser#Literal.
ExprListener.prototype.exitLiteral = function(ctx) {
    ctx._result = parseInt(ctx.getText());
};


exports.ExprCompiler = ExprCompiler;